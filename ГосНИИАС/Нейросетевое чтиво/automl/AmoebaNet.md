AmoebaNet

[Оглавление](../../../ГосНИИАС/Нейросетевое%20чтиво/automl/Оглавление.md)
AmoebaNet
[Collab](https://colab.research.google.com/github/google-research/google-research/blob/master/evolution/regularized_evolution_algorithm/regularized_evolution.ipynb)
# intro
Использует пространство поиска NASNet, с модифицированным генетическим алгоритмом ![1a4a0f5c5f111c3f5a149e16150792a0.png](../../../_resources/332d790cdbd1419984b6a2b423dc17a7.png)
# Модифицированный генетический алгоритм Aging evolution
## Контроль популяции
Там не только турнир, но и учёт возвраста, причём не возвраст отдельных слоёв [ALPS](http://gpbib.cs.ucl.ac.uk/gecco2006/docs/p815.pdf "ALPS: The Age-Layered Population Structure for Reducing the Problem of Premature Convergence"), для того, чтобы старые не конкурировали с молодыми а возвраст индивидов, сетей, для того, чтобы удалять старых из популяции и поддерживать постоянный размер. Также это позволяет отсеивать тех, кому повезло получить хорошую оценку, однако совокупность его генов не имеет дальнейшего развития, является тупиковой веткой, или случайно получились хорошие веса, хотя в среднем данная сетка плохо учится.
## Мутации
- hidden state mutation
Сначала выбирается что модифицировать: normal или reduction cell, далее выбирается одна из пяти(??) возможных комбинаций. После этого одно или два состояния заменяются, при этом следя, чтобы не было петли.
- op mutation
Также случайно выбирается объект преобразования, но заменяется оператор.
- identity
![c453ed5f804bd9bd0356d015f7069d59.png](../../../_resources/c397b6bf9bbb4d9887e1ca9045784c2a.png)
## baseline для сравнения с RL
Взяли отсюда [NASNet](../../../ГосНИИАС/Нейросетевое%20чтиво/automl/NASNet.md) для RL с LSTM, сами написали для Random Search (RS), RS позволил сделать архитектуры равновероятными, а не создавая из предыдущих. Эксперименты проводили на CIFAR-10
 Possible ops:
none (identity); 3x3, 5x5 and 7x7 separable (sep.) convo-
lutions (convs.); 3x3 average (avg.) pool; 3x3 max pool;
3x3 dilated (dil.) sep. conv.; 1x7 then 7x1 conv.
## Сравнение Evlolution с RL и RS
Evolution показывает большую вычислительную эффективность, особенно на ранних стадиях,
![38f00ab9b21b9628dac133b6597deea5.png](../../../_resources/524b0083ad0345ce83b33bddd15a438e.png)
![40376a22771f4c693805369ebf42a920.png](../../../_resources/58f5c0b058614048811c3bcbe37b1e88.png)
Поиск происходил на небольших моделях, которые разворачивались в большие, *augmentatuin trick* [NASNet](../../../ГосНИИАС/Нейросетевое%20чтиво/automl/NASNet.md)
## Result arch -- AmoebaNet
![b7206dc4f3a8c8fdee5dde37e9a4a387.png](../../../_resources/118ae85c3798432eaf8b9068dadd7864.png)
Сравнение с NASNet
![e02244406a4ab83cb653635f953f75d3.png](../../../_resources/a7cfd242ce7a4e5ab14246229bcb9f8e.png)
Сравнение на ImageNet
![4a8790a6e49c1b12385bcefb1a713a73.png](../../../_resources/cca5741e479a4a43bd83e91a2119d0c7.png)
[Оглавление](../../../ГосНИИАС/Нейросетевое%20чтиво/automl/Оглавление.md)